import PostSubmit from "./components/PostSubmit";
import {BrowserRouter, Route} from "react-router-dom";
import {AuthorInfo_1, AuthorInfo_2, AuthorInfo_3, AuthorInfo_0}
    from "./components/AuthorPost";


function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Route exact path='/' component={PostSubmit}/>
                <Route exact path='/authors/' component={AuthorInfo_0}/>
                <Route path='/authors/User1 surnameUser1' component={AuthorInfo_1}/>
                <Route path='/authors/User2 surnameUser2' component={AuthorInfo_2}/>
                <Route exact path='/authors/User3 surnameUser3' component={AuthorInfo_3}/>

            </div>
        </BrowserRouter>
    );
}

export default App;
