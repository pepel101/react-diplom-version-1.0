import React, {useState} from "react";
import PostForm from "./PostForma";
import {NavLink} from "react-router-dom";
import style from './Styles/Post.module.css'


function Post({posts, removePost, editPost, state}) {
    const [edit, setEdit] = useState({
        id: null,
        value: ''
    })

    const save = (value) => {
        editPost(edit.id, value)
        setEdit({
            id: null,
            value: ''
        })
    }
    if (edit.id) {
        return (
            <div id="positionForm">
                <PostForm edit={edit} state={state} onSubmit={save}/>
            </div>
        )
    }

    return (
        posts.map((post, index) => (
            <div key={index} className={style.post}>
                <div className={style.headPost}>
                    <NavLink to={'/authors/' + post.author} key={post.id}
                             className="contentAuthor">{post.author}&#10148;&#10148;</NavLink>
                    <div key={post.id + "c"} className={style.profileDate}>{post.postDate}</div>
                </div>
                <div key={post.id + "a"} className={style.profileHeading}>
                    <h2>{post.heading}</h2>
                </div>
                <div key={post.id + "b"} className={style.profileText}>{post.text}</div>
                <div className={style.blockPostButton}>
                    <button type="button" className={style.editButton} onClick={() =>
                        setEdit({
                            id: post.id,
                            value: ''
                        })}>Edit
                    </button>
                    <button type="button" onClick={() => removePost(post.id)}>Delete</button>
                </div>
            </div>
        ))
    )
}

export default Post