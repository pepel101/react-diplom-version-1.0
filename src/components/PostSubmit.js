import React, {useState} from "react";
import PostForm from "./PostForma";
import Post from "./Post";
import {state} from "../state/State";
import {StopButton} from "./StopButton";
import style from "./Styles/PostSubmit.module.css"


function PostSubmit() {
    const [posts, setPosts] = useState([])

    const addPost = post => {
        const allPosts = [post, ...posts]

        setPosts(allPosts)
        console.log(allPosts)
    }

    const editPost = (postId, newValue) => {
        setPosts(prev => prev.map(item => (item.id === postId ? newValue : item)))
    }

    const removePost = (id) => {
        const newPosts = [...posts].filter(post => post.id !== id)
        setPosts(newPosts)
    }

    const sortAuthor = () => {
        const sortPost = [...posts].sort(function (a, b) {
            if (a.author < b.author) {
                return -1;
            }
            if (a.author > b.author) {
                return 1;
            }
            return 0;
        })
        setPosts(sortPost)

    }

    const sortAuthor2 = () => {
        const sortPost = [...posts].sort(function (a, b) {
            if (a.author > b.author) {
                return -1;
            }
            if (a.author < b.author) {
                return 1;
            }
            return 0;
        })
        setPosts(sortPost)

    }

    const sortHeading = () => {
        const sortPost = [...posts].sort(function (a, b) {
            if (a.heading < b.heading) {
                return -1;
            }
            if (a.heading > b.heading) {
                return 1;
            }
            return 0;
        })
        setPosts(sortPost)
    }

    const sortHeading2 = () => {
        const sortPost = [...posts].sort(function (a, b) {
            if (a.heading > b.heading) {
                return -1;
            }
            if (a.heading < b.heading) {
                return 1;
            }
            return 0;
        })
        setPosts(sortPost)
    }

    const sortText = () => {
        const sortPost = [...posts].sort(function (a, b) {
            if (a.text < b.text) {
                return -1;
            }
            if (a.text > b.text) {
                return 1;
            }
            return 0;
        })
        setPosts(sortPost)
    }

    const sortText2 = () => {
        const sortPost = [...posts].sort(function (a, b) {
            if (a.text > b.text) {
                return -1;
            }
            if (a.text < b.text) {
                return 1;
            }
            return 0;
        })
        setPosts(sortPost)
    }

    const sortDate = () => {
        const sortPost = [...posts].sort(function (a, b) {
            if (a.postDate < b.postDate) {
                return -1;
            }
            if (a.postDate > b.postDate) {
                return 1;
            }
            return 0;
        })
        setPosts(sortPost)
    }

    const sortDate2 = () => {
        const sortPost = [...posts].sort(function (a, b) {
            if (a.postDate > b.postDate) {
                return -1;
            }
            if (a.postDate < b.postDate) {
                return 1;
            }
            return 0;
        })
        setPosts(sortPost)
    }


    return (
        <div className="mainDiv">
            <div className={style.blockH1}>
                <h1>News feed</h1>
            </div>
            <div className={style.actionBlock}>
                <div>
                    <PostForm onSubmit={addPost} state={state}/>
                    <StopButton
                        sortAuthor={sortAuthor}
                        sortHeading={sortHeading}
                        sortText={sortText}
                        sortDate={sortDate}
                        sortAuthor2={sortAuthor2}
                        sortHeading2={sortHeading2}
                        sortText2={sortText2}
                        sortDate2={sortDate2}
                    />
                </div>
                <div className={style.blockPosts}>

                    <Post posts={posts}
                          removePost={removePost}
                          editPost={editPost}
                          state={state}
                    />
                </div>
            </div>
        </div>
    )
}

export default PostSubmit