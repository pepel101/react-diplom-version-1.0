import React from "react";
import {state} from "../state/State";
import {NavLink} from "react-router-dom";
import style from "./Styles/AutorPost.module.css"


export function AuthorInfo_0() {
    return (
        <div className={style.allAuthorsPage}>
            <NavLink to="/">Назад</NavLink>
            <div className={style.authorsBlock}>
                <h1>{state[0].name + '' + state[0].surname}</h1>
                <p>возраст: {state[0].age}</p>
                <p>адрес: город {state[0].city}, улица {state[0].street}, дом {state[0].house}</p>
                <p>почта: {state[0].email}</p>
                <p>образование: {state[0].education}</p>
            </div>
            <div className={style.authorsBlock}>

                <h1>{state[1].name + '' + state[1].surname}</h1>
                <p>возраст: {state[1].age}</p>
                <p>адрес: город {state[1].city}, улица {state[1].street}, дом {state[1].house}</p>
                <p>почта: {state[1].email}</p>
                <p>образование: {state[1].education}</p>
            </div>
            <div className={style.authorsBlock}>

                <h1>{state[2].name + '' + state[2].surname}</h1>
                <p>возраст: {state[2].age}</p>
                <p>адрес: город {state[2].city}, улица {state[2].street}, дом {state[2].house}</p>
                <p>почта: {state[2].email}</p>
                <p>образование: {state[2].education}</p>
            </div>
            <div className={style.authorsBlock}>

                <h1>{state[3].name + '' + state[3].surname}</h1>
                <p>возраст: {state[3].age}</p>
                <p>адрес: город {state[3].city}, улица {state[3].street}, дом {state[3].house}</p>
                <p>почта: {state[3].email}</p>
                <p>образование: {state[3].education}</p>
            </div>

        </div>
    )

}

export function AuthorInfo_1() {
    return (
        <>
            <div className={style.authorsBlock}>

                <h1>{state[0].name + '' + state[0].surname}</h1>
                <p>возраст: {state[0].age}</p>
                <p>адрес: город {state[0].city}, улица {state[0].street}, дом {state[0].house}</p>
                <p>почта: {state[0].email}</p>
                <p>образование: {state[0].education}</p>
                <NavLink to="/">Назад</NavLink>
            </div>
        </>
    )

}

export function AuthorInfo_2() {
    return (
        <>
            <div className={style.authorsBlock}>

                <h1>{state[1].name + '' + state[1].surname}</h1>
                <p>возраст: {state[1].age}</p>
                <p>адрес: город {state[1].city}, улица {state[1].street}, дом {state[1].house}</p>
                <p>почта: {state[1].email}</p>
                <p>образование: {state[1].education}</p>
                <NavLink to="/">Назад</NavLink>
            </div>
        </>
    )

}

export function AuthorInfo_3() {
    return (
        <>
            <div className={style.authorsBlock}>

                <h1>{state[2].name + ' ' + state[2].surname}</h1>
                <p>возраст: {state[2].age}</p>
                <p>адрес: город {state[2].city}, улица {state[2].street}, дом {state[2].house}</p>
                <p>почта: {state[2].email}</p>
                <p>образование: {state[2].education}</p>
                <NavLink to="/">Назад</NavLink>
            </div>
        </>
    )

}


    
    




